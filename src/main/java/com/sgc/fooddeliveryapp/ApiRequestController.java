package com.sgc.fooddeliveryapp;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sgc.fooddeliveryapp.calculator.DeliveryFeeCalculator;
import com.sgc.fooddeliveryapp.calculator.Vehicle;
import com.sgc.fooddeliveryapp.weather.Region;

@RestController
public class ApiRequestController {

	@Autowired
	private DeliveryFeeCalculator calculator;

	/**
	 * Get price of delivery from given input values
	 * 
	 * path= /api/calculator?...
	 * params:
	 * region=? (valid: tallinn, tartu, pärnu)
	 * vehicle=? (valid: car, scooter, bike)
	 * timestamp=? (optional, long value)
	 * 
	 * If timestamp is invalid or no data is avaliable for that time, then latest weatherdata will be returned
	 * If parameters are false or vehicle usage is prohibited, an error message will be returned
	 * 
	 * Output is in JSON format
	 * Example:
	 * {
	 * 	"result":"4.0",
	 * 	"region":"TALLINN",
	 * 	"vehicle":"CAR"
	 * }
	 * @param region Target region
	 * @param vehicle Vehicle preferance
	 * @param timestamp Optional timestamp parameter
	 * @return Price in JSON format
	 */
    @GetMapping(value = "/api/calculator", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Map<String, String>> getPrice(@RequestParam(name = "region", required = true) String region, 
		@RequestParam(name = "vehicle", required = true) String vehicle,
		@RequestParam(name = "timestamp", required = false) Long timestamp) {
		
		HttpHeaders headers = new HttpHeaders();
		String result = calculator.getDeliveryFee(
			Region.getRegionBySlug(region), 
			Vehicle.findByName(vehicle),
			timestamp);
		Map<String, String> output = new HashMap<>();
		output.put("region", region);
		output.put("vehicle", vehicle);
		output.put("result", result);
		return new ResponseEntity<>(output, headers, HttpStatus.CREATED);
	}

}
