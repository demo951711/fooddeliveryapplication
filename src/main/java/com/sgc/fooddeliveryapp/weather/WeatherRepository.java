package com.sgc.fooddeliveryapp.weather;

import java.io.Serializable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface WeatherRepository extends CrudRepository<WeatherData, Serializable> {
    
    /**
     * Selects latest weather report from repository, specified by weather station name, and creates a WeatherData object from it
     * @param name Weather station name
     * @return WeatherData object
     */
    @Query(value = "SELECT u FROM WeatherData u WHERE u.wmoCode = :wmocode ORDER BY u.timestamp DESC LIMIT 1")
    WeatherData getDataByWmo(@Param("wmocode") int wmocode);

    /**
     * Selects weather report from repository, specified by weather station name and timestamp to start looking from, and creates a WeatherData object from it
     * @param name Weather station name
     * @param timestamp Timestamp where to start matching
     * @return WeatherData object
     */
    @Query(value = "SELECT u FROM WeatherData u WHERE u.wmoCode = :wmocode AND u.timestamp <= :timestamp ORDER BY u.timestamp DESC LIMIT 1")
    WeatherData getDataByWmoAndTimestamp(@Param("wmocode") int wmocode, @Param("timestamp") Long timestamp);

}
