package com.sgc.fooddeliveryapp.weather;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sgc.fooddeliveryapp.weather.exceptions.XMLParseException;

public final class XMLParser {

    /**
     * Parses XML data from target input stream
     * @param fileName File name
     * @return Parsed weather data put into a HashMap
     */
    public Map<Integer, WeatherData> parseFile(final InputStream stream) {
        Map<Integer, WeatherData> weatherMap = new HashMap<>();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLHandler handler = new XMLHandler();
            parser.parse(stream, handler);
            weatherMap = handler.getData();
        } catch (SAXException | ParserConfigurationException | IOException e) {
            throw new XMLParseException(e, "Failed to parse XML data");
        }

        return weatherMap;
    }

    private class XMLHandler extends DefaultHandler {

        private String[] locations = {"Tallinn-Harku", "Tartu-Tõravere", "Pärnu"};
        private String[] validElements = {"name", "wmocode", "phenomenon", "airtemperature", "windspeed"};
        private Long timestamp;
        private Map<Integer, WeatherData> data;
        private Map<String, String> elements;
        private String currentElement;

        private boolean station = false;
        private boolean name = false;

        @Override
        public void startDocument() throws SAXException {
            data = new HashMap<>();
            elements = new HashMap<>();
        }

        /**
         * Start of an element ie. <qName attribute=value>
         */
        @Override
        public void startElement(String uri, String lName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("observations")) {
                timestamp = Long.valueOf(attributes.getValue("timestamp"));
            } else if (qName.equals("station")) {
                station = true;
            } else if (qName.equals("name")) {
                name = true;
            }
            currentElement = qName;
        }

        /**
         * Whatever is in between element nodes
         */
        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (station) {
                String str = new String(ch, start, length).trim();
                if (name && !Arrays.asList(locations).contains(str)) {
                    station = false;
                    return;
                }
                if (Arrays.asList(validElements).contains(currentElement)) {
                    elements.computeIfAbsent(currentElement, k -> str);
                }
            }

        }

        /**
         * </qName> - end of a XML element
         */
        @Override
        public void endElement(String uri, String lName, String qName) throws SAXException {
            if (qName.equals("station") && station) {
                WeatherData weatherData = new WeatherData();
                weatherData.setTimestamp(timestamp);
                weatherData.setName(elements.get("name"));
                weatherData.setWmoCode(Integer.parseInt(elements.get("wmocode")));
                weatherData.setPhenomenom(elements.get("phenomenon"));
                weatherData.setAirTemp(Double.parseDouble(elements.get("airtemperature")));
                weatherData.setWindSpeed(Double.parseDouble(elements.get("windspeed")));
                data.put(weatherData.getWmoCode(), weatherData);
                elements.clear();
                station = false;
            } else if (qName.equals("name")) {
                name = false;
            }
        }

        public Map<Integer, WeatherData> getData() {
            return data;
        }
    }

}
