package com.sgc.fooddeliveryapp.weather;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.sgc.fooddeliveryapp.weather.exceptions.MissingXMLException;

import lombok.Getter;
import lombok.AccessLevel;

@Service
public class WeatherScraper {

    @Autowired
    private WeatherRepository repository;
    @Getter(AccessLevel.PUBLIC)
    private Map<Integer, WeatherData> currentData = new HashMap<>();

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(cron = "${cron.expression}")
    private void fetchCurrentData() {
        fetchDataFromURL("https://www.ilmateenistus.ee/ilma_andmed/xml/observations.php");
    }
    
    /**
     * Fetches weather data from given input url
     * @param url input URL
     */
    public void fetchDataFromURL(String url) {
        try (BufferedInputStream inputStream = new BufferedInputStream(new URL(url).openStream());) {
            XMLParser parser = new XMLParser();
            Map<Integer, WeatherData> newData = parser.parseFile(inputStream);
            for (WeatherData wd : newData.values()) {
                repository.save(wd);
                if (currentData.get(wd.getWmoCode()) == null || currentData.get(wd.getWmoCode()).getTimestamp() < wd.getTimestamp()) {
                    currentData.put(wd.getWmoCode(), wd);
                }
            }
        } catch (IOException e) {
            throw new MissingXMLException(e, "Failed to fetch weather report XML file.");
        } 
    }

    /**
     * Returns weather data from region closest to said timestamp, if timestamp is invalid, returns current data
     * @param station Weather station WMO code
     * @param timestamp Timestamp when data was recorded
     * @return WeatherData instance
     */
    public WeatherData getWeatherDataByTimestamp(int wmocode, Long timestamp) {
        WeatherData data = repository.getDataByWmoAndTimestamp(wmocode, timestamp);
        if (data == null) {
            return getWeatherData(wmocode);
        }
        return data;
    }

    /**
     * Request data from region, if data isn't in memory, it will be loaded from database
     * @param region Weather station WMO Code
     * @return WeatherData instance
     */
    public WeatherData getWeatherData(int wmocode) {
        if (currentData.containsKey(wmocode)) {
            return currentData.get(wmocode);
        }
        return repository.getDataByWmo(wmocode);
    }

}
