package com.sgc.fooddeliveryapp.weather;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Entity
public class WeatherData implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter (AccessLevel.PUBLIC)
    private int id;

    @Column(name = "name")
    @Getter (AccessLevel.PUBLIC)
    @Setter (AccessLevel.PUBLIC)
    private String name;

    @Column(name = "wmo_code")
    @Getter (AccessLevel.PUBLIC)
    @Setter (AccessLevel.PUBLIC)
    private int wmoCode;

    @Column(name = "air_temp")
    @Getter (AccessLevel.PUBLIC)
    @Setter (AccessLevel.PUBLIC)
    private double airTemp;

    @Column(name = "wind_speed")
    @Getter (AccessLevel.PUBLIC)
    @Setter (AccessLevel.PUBLIC)
    private double windSpeed;

    @Column(name = "phenomenom")
    @Getter (AccessLevel.PUBLIC)
    @Setter (AccessLevel.PUBLIC)
    private String phenomenom;

    @Column(name = "timestamp")
    @Getter (AccessLevel.PUBLIC)
    @Setter (AccessLevel.PUBLIC)
    private Long timestamp;

    @Override
    public String toString() {
        return "WeatherData{" +
        "name='" + name + "'' wmoCode=" + wmoCode + " airTemp=" + airTemp + 
        " windSpeed=" + windSpeed + " phenomenom='" + phenomenom + "'' timestamp=" + timestamp + "}";
    }
    
}
