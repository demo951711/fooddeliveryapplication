package com.sgc.fooddeliveryapp.weather;

import lombok.Getter;

public enum Region {
        
    TALLINN("tallinn", "Tallinn-Harku", 26038, new double[] {4.0, 3.5, 3.0}),
    TARTU("tartu", "Tartu-Tõravere", 26242, new double[] {3.5, 3.0, 2.5}),
    PARNU("pärnu", "Pärnu", 41803, new double[] {3.0, 2.5, 2.0});

    Region(String slug, String weatherStation, int wmoCode, double[] fees) {
        this.slug = slug;
        this.weatherStation = weatherStation;
        this.wmoCode = wmoCode;
        this.fees = fees;   // 0 = CAR, 1 = SCOOTER, 2 = BIKE
    }

    @Getter 
    String slug;
    @Getter 
    String weatherStation;
    @Getter
    int wmoCode;
    @Getter
    double[] fees;

    /**
     * Get region by url slug (region=slug)
     * @param slug Region slug
     * @return Region
     */
    public static Region getRegionBySlug(String slug) {
        for (Region region : Region.values()) {
            if (region.getSlug().equalsIgnoreCase(slug)) {
                return region;
            }
        }
        return null;
    }

}
