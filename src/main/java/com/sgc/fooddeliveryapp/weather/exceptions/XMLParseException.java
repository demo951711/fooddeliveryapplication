package com.sgc.fooddeliveryapp.weather.exceptions;

public class XMLParseException extends RuntimeException {
    
    public XMLParseException(Throwable cause, String message) {
        super(message, cause);
    }
    
}
