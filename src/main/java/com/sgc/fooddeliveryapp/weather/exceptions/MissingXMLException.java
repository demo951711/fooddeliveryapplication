package com.sgc.fooddeliveryapp.weather.exceptions;

public class MissingXMLException extends RuntimeException {
    
    public MissingXMLException(Throwable cause, String message) {
        super(message, cause);
    }

}
