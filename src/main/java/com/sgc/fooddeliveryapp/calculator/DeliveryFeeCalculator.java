package com.sgc.fooddeliveryapp.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sgc.fooddeliveryapp.weather.Region;
import com.sgc.fooddeliveryapp.weather.WeatherData;
import com.sgc.fooddeliveryapp.weather.WeatherScraper;

@Component
public class DeliveryFeeCalculator {
    
    @Autowired
    private WeatherScraper weather;

    /**
     * Calculates the delivery fee based on location and type of vehicle.
     * If calculateExtraFee returns -1.0 then that vehicle type is forbidden to be used.
     * @param region Region enum
     * @param vehicle Vehicle enum
     * @param timestamp Optional timestamp variable
     * @return Calculated delivery fee
     */
    public String getDeliveryFee(Region region, Vehicle vehicle, Long timestamp) {
        if (region == null || vehicle == null) {
            return "Invalid input parameters";
        }
        double baseFee = getBaseFee(region, vehicle);
        double extraFee = calculateExtraFee(region, vehicle, timestamp);
        if (extraFee == -1) {
            return "Usage of selected vehicle type is forbidden";
        }
        return String.valueOf(baseFee + extraFee);
    }

    /**
     * Returns base fee, depending on region and vehicl
     * @param region Region enum
     * @param vehicle Vehicle enum
     * @return
     */
    private double getBaseFee(Region region, Vehicle vehicle) {
        switch (vehicle) {
            case CAR:
                return region.getFees()[0];
            case SCOOTER:
                return region.getFees()[1];
            case BIKE:
                return region.getFees()[2];
            default:
                return 0.0;
        }
    }

    /**
     * Calculates extra fee taking weather into consideration
     * If -1.0 is returned then that vehicle type is forbidden to be used
     * @param region Region enum
     * @param vehicle Vehicle enum
     * @return Calculated extra fee
     */
    private double calculateExtraFee(Region region, Vehicle vehicle, Long timestamp) {
        if (vehicle == Vehicle.CAR) return 0.0;
        double result = 0.0;
        WeatherData data = timestamp == null ? 
                weather.getWeatherData(region.getWmoCode()) : 
                weather.getWeatherDataByTimestamp(region.getWmoCode(), timestamp);

        if (data.getAirTemp() < 0) {
            result += (data.getAirTemp() < -10) ? 1.0 : 0.5;
        }

        switch(data.getPhenomenom().toLowerCase()) {
            case "light snow shower":
            case "moderate snow shower":
            case "heavy snow shower":
            case "light sleet":
            case "moderate sleet":
            case "light snowfall":
            case "moderate snowfall":
            case "heavy snowfall":
            case "blowing snow":
            case "drifting snow":
                result += 1.0;
                break;
            case "light shower":
            case "moderate shower":
            case "heavy shower":
            case "light rain":
            case "moderate rain":
            case "heavy rain":
                result += 0.5;
                break;
            case "thunder":
            case "thunderstorm":
            case "glaze":
            case "hail":
                return -1.0;
            default:
                break;
        }

        if (vehicle == Vehicle.BIKE) {
            if (10 <= data.getWindSpeed() && data.getWindSpeed() <= 20) {
                result += 0.5;
            } else if (data.getWindSpeed() > 20) {
                return -1.0;
            }
        }
        return result;
    }

}
