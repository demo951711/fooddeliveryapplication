package com.sgc.fooddeliveryapp.calculator;

public enum Vehicle {
    
    CAR("Car"), SCOOTER("Scooter"), BIKE("Bike");

    Vehicle(String displayName) {
        this.displayName = displayName;
    }

    String displayName;

    /**
     * Search for Vehicle by inout string
     * @param name Enum name to look for
     * @return Vehicle
     */
    public static Vehicle findByName(String name) {
        for (Vehicle vehicle : Vehicle.values()) {
            if (vehicle.name().equalsIgnoreCase(name)) {
                return vehicle;
            }
        }
        return null;
    }

}
