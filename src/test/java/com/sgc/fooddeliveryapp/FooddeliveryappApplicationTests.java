package com.sgc.fooddeliveryapp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.net.MalformedURLException;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sgc.fooddeliveryapp.calculator.DeliveryFeeCalculator;
import com.sgc.fooddeliveryapp.calculator.Vehicle;
import com.sgc.fooddeliveryapp.weather.Region;
import com.sgc.fooddeliveryapp.weather.WeatherData;
import com.sgc.fooddeliveryapp.weather.WeatherRepository;
import com.sgc.fooddeliveryapp.weather.WeatherScraper;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class FooddeliveryappApplicationTests {

	@Autowired
	WeatherScraper scraper;

	@Autowired
	DeliveryFeeCalculator calculator;

	@Autowired
	WeatherRepository repository;

	@Test
	@Order(1)
	void XMLFileParses() {
		assertEquals(3, scraper.getCurrentData().size());
		assertTrue(scraper.getCurrentData().containsKey(Region.TALLINN.getWmoCode()));
		assertTrue(scraper.getCurrentData().containsKey(Region.TARTU.getWmoCode()));
		assertTrue(scraper.getCurrentData().containsKey(Region.PARNU.getWmoCode()));
	}

	@Test
	@Order(2)
	void XMLParseDataGetsPutIntoDatabase() {
		assertEquals(3, repository.count());
	}

	@Test
	void DeliveryFeeCalculatorInvalidDataGivesErrorMessage() {
		assertEquals("Invalid input parameters", calculator.getDeliveryFee(Region.getRegionBySlug("aaaaa"), Vehicle.findByName("bbbb"), null));
	}

	// From this point on we just manipulate the currentData map to test specific cases

	@Test
	void DeliveryFeeCalculationRegularTallinn() {
		WeatherData data = new WeatherData();
		data.setName(Region.TALLINN.getWeatherStation());
		data.setWmoCode(Region.TALLINN.getWmoCode());
		data.setAirTemp(0);
		data.setWindSpeed(0);
		data.setPhenomenom("clear");
		data.setTimestamp(scraper.getWeatherData(Region.TALLINN.getWmoCode()).getTimestamp());
		scraper.getCurrentData().put(data.getWmoCode(), data);

		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("3.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
	}

	@Test
	void DeliveryFeeCalculationRegularTartu() {
		WeatherData data = new WeatherData();
		data.setName(Region.TARTU.getWeatherStation());
		data.setWmoCode(Region.TARTU.getWmoCode());
		data.setAirTemp(0);
		data.setWindSpeed(0);
		data.setPhenomenom("clear");
		data.setTimestamp(scraper.getWeatherData(Region.TARTU.getWmoCode()).getTimestamp());
		scraper.getCurrentData().put(data.getWmoCode(), data);

		assertEquals("3.5", calculator.getDeliveryFee(Region.TARTU, Vehicle.CAR, null));
		assertEquals("3.0", calculator.getDeliveryFee(Region.TARTU, Vehicle.SCOOTER, null));
		assertEquals("2.5", calculator.getDeliveryFee(Region.TARTU, Vehicle.BIKE, null));
	}

	@Test
	void DeliveryFeeCalculationRegularParnu() {
		WeatherData data = new WeatherData();
		data.setName(Region.PARNU.getWeatherStation());
		data.setWmoCode(Region.PARNU.getWmoCode());
		data.setAirTemp(0);
		data.setWindSpeed(0);
		data.setPhenomenom("clear");
		data.setTimestamp(scraper.getWeatherData(Region.PARNU.getWmoCode()).getTimestamp());
		scraper.getCurrentData().put(data.getWmoCode(), data);

		assertEquals("3.0", calculator.getDeliveryFee(Region.PARNU, Vehicle.CAR, null));
		assertEquals("2.5", calculator.getDeliveryFee(Region.PARNU, Vehicle.SCOOTER, null));
		assertEquals("2.0", calculator.getDeliveryFee(Region.PARNU, Vehicle.BIKE, null));
	}

	@Test
	void DeliveryFeeCalculationVehicleUsageForbiddenPhenomenom() {
		WeatherData data = scraper.getWeatherData(Region.TALLINN.getWmoCode());
		data.setAirTemp(0);
		data.setWindSpeed(0);

		data.setPhenomenom("Thunder");
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));
		
		data.setPhenomenom("Thunderstorm");
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setPhenomenom("Glaze");
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setPhenomenom("Hail");
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));
	}

	@Test
	void DeliveryFeeCalculationSnowAffectsPrice() {
		WeatherData data = scraper.getWeatherData(Region.TALLINN.getWmoCode());
		data.setAirTemp(0);
		data.setWindSpeed(0);

		data.setPhenomenom("Light snow shower");
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("4.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));
	}

	@Test	
	void DeliveryFeeCalculationRainAffectsPrice() {
		WeatherData data = scraper.getWeatherData(Region.TALLINN.getWmoCode());
		data.setAirTemp(0);
		data.setWindSpeed(0);

		data.setPhenomenom("Moderate shower");
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setPhenomenom("Moderate rain");
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));
	}

	@Test
	void DeliveryFeeCalculationAirTempAffectsPrice() {
		WeatherData data = scraper.getWeatherData(Region.TALLINN.getWmoCode());
		data.setAirTemp(13);
		data.setWindSpeed(0);
		data.setPhenomenom("Clear");

		assertEquals("3.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setAirTemp(-2);
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setAirTemp(-21);
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("4.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));
	}

	@Test
	void DeliveryFeeCalculationWindAffectsPrice() {
		WeatherData data = scraper.getWeatherData(Region.TALLINN.getWmoCode());
		data.setAirTemp(0);
		data.setPhenomenom("Clear");

		data.setWindSpeed(6);
		assertEquals("3.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setWindSpeed(13);
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setWindSpeed(20);
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));

		data.setWindSpeed(21);
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, null));
		assertEquals("3.5", calculator.getDeliveryFee(Region.TALLINN, Vehicle.SCOOTER, null));
		assertEquals("4.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.CAR, null));
	}

	@Test
	void XMLParsePastTimestampDoesntOverrideCurrentDtata() throws MalformedURLException {
		WeatherData data1 = scraper.getWeatherData(Region.TALLINN.getWmoCode());
		WeatherData data2 = scraper.getWeatherData(Region.TARTU.getWmoCode());
		WeatherData data3 = scraper.getWeatherData(Region.PARNU.getWmoCode());

		File testXml = new File("src/test/java/com/sgc/fooddeliveryapp/xml/test-data-1.xml");
		scraper.fetchDataFromURL(testXml.toURI().toURL().toString());

		assertSame(data1, scraper.getWeatherData(Region.TALLINN.getWmoCode()));
		assertSame(data2, scraper.getWeatherData(Region.TARTU.getWmoCode()));
		assertSame(data3, scraper.getWeatherData(Region.PARNU.getWmoCode()));
	}

	@Test
	void XMLParseDataGetsPutIntoDatabase2() {
		assertEquals(6, repository.count());
	}

	@Test
	void DeliveryFeeCalculationPastTimestamp() {
		assertEquals("5.0", calculator.getDeliveryFee(Region.TALLINN, Vehicle.BIKE, Long.valueOf(1679220550))); // pin-point accurate timestamp
		assertEquals("3.5", calculator.getDeliveryFee(Region.TARTU, Vehicle.SCOOTER, Long.valueOf(1679220569))); // a bit higher timestamp
		Long currentTimestamp = scraper.getWeatherData(Region.PARNU.getWmoCode()).getTimestamp();
		assertEquals("Usage of selected vehicle type is forbidden", calculator.getDeliveryFee(Region.PARNU, Vehicle.SCOOTER, currentTimestamp - 1L)); // Current data timestamp -1
		assertNotSame(scraper.getWeatherData(Region.TALLINN.getWmoCode()), scraper.getWeatherDataByTimestamp(Region.TALLINN.getWmoCode(), 1679220550L)); // Not same
	}

}
